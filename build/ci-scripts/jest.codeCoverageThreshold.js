// @flow

module.exports = {
  'packages/core/navigation-next/src': {
    statements: 78,
    branches: 69,
    functions: 78,
    lines: 79,
  },
  'packages/core/global-navigation/src': {
    statements: 90,
    branches: 90,
    functions: 90,
    lines: 90,
  },
  'packages/media/media-image/src': {
    statements: 100,
    branches: 100,
    functions: 100,
    lines: 100,
  },
  'packages/media/media-core/src': {
    statements: 96,
    branches: 92,
    functions: 91,
    lines: 97,
  },
  'packages/media/media-card/src': {
    statements: 92,
    branches: 86,
    functions: 90,
    lines: 92,
  },
  'packages/media/media-filmstrip/src': {
    statements: 87,
    branches: 85,
    functions: 79,
    lines: 89,
  },
  'packages/media/media-picker/src': {
    statements: 86,
    branches: 72,
    functions: 78,
    lines: 86,
  },
  'packages/media/media-store/src': {
    statements: 91,
    branches: 80,
    functions: 89,
    lines: 91,
  },
  'packages/media/media-ui/src': {
    statements: 89,
    branches: 79,
    functions: 82,
    lines: 88,
  },
  'packages/media/media-viewer/src': {
    statements: 91,
    branches: 82,
    functions: 92,
    lines: 90,
  },
  'packages/media/smart-card/src': {
    statements: 84,
    branches: 73,
    functions: 55,
    lines: 83,
  },
  'packages/media/media-avatar-picker/src': {
    statements: 88,
    branches: 83,
    functions: 75,
    lines: 89,
  },
};
