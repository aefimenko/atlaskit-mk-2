# @atlaskit/icon-priority

## 3.0.0
- [major] [ecf21be58f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ecf21be58f):

  - Update the size of blocker and trivial icon to 20px from 18px.

## 2.0.0
- [major] [a2653f4453](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a2653f4453):

  - Update the export name for blocker icon to priority-blocker from priotity-blocker

## 1.0.0
- [major] [d0333acfba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d0333acfba):

  - First release of the priority icons package
