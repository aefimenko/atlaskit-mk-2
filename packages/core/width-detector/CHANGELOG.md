# @atlaskit/width-detector

## 0.2.0
- [minor] [6cc3a8fea4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6cc3a8fea4):

  - Initial version of the WidthDetector component, modeled on the SizeDetector component.
