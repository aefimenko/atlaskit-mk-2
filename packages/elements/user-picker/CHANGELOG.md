# @atlaskit/user-picker

## 1.0.21
- [patch] [b38b2098e3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b38b2098e3):

  - FS-3417 export utils functions

## 1.0.20
- Updated dependencies [06713e0a0c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/06713e0a0c):
  - @atlaskit/select@7.0.0

## 1.0.19
- [patch] [1050084e29](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1050084e29):

  - TEAMS-242 : Change user picker placeholder

## 1.0.18
- [patch] [0809a67d7b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0809a67d7b):

  - FS-3591 hide selected users from multi picker

## 1.0.17
- [patch] [67f0d11134](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/67f0d11134):

  - FS-3577 show selected options by default

## 1.0.16
- [patch] [c51d1e2e51](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c51d1e2e51):

  - FS-3573 show user avatar on focus

## 1.0.15
- [patch] [1ce3a8812b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1ce3a8812b):

  - FS-3458 call loadOptions if open prop is controlled
