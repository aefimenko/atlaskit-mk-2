# @atlaskit/xregexp-transformer

## 1.1.0
- [minor] [1d19234fbd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1d19234fbd):

  - Enable noImplicitAny and resolve issues for elements util packages

## 1.0.0
- [major] [0f19693](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/0f19693):

  - added tests for xregexp transformer, updated README and simplified code
- [major] [b789b3a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b789b3a):

  - removed xregexp library dependency from emoji and mention components, added xregexp-transformer package to compile xregexp expressions to unicode charsets
