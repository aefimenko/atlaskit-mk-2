import Example from './08-resourced-mention-on-n20-background';
import createHydrateExample from '../example-helpers/demo-ssr-hydrate';

export default createHydrateExample(Example);
