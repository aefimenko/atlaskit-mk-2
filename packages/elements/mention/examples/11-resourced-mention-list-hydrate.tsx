import Example from './04-resourced-mention-list';
import createHydrateExample from '../example-helpers/demo-ssr-hydrate';

export default createHydrateExample(Example);
