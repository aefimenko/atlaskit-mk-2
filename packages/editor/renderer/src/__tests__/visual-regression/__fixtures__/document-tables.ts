const wideTableResized = {
  version: 1,
  type: 'doc',
  content: [
    {
      type: 'table',
      attrs: {
        isNumberColumnEnabled: false,
        layout: 'wide',
      },
      content: [
        {
          type: 'tableRow',
          content: [
            {
              type: 'tableHeader',
              attrs: {
                colwidth: [149],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'text',
                      text: 'Product',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableHeader',
              attrs: {
                colwidth: [108],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'text',
                      text: 'Editor',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableHeader',
              attrs: {
                colwidth: [107],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'text',
                      text: 'Renderer',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableHeader',
              attrs: {
                colwidth: [129],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'text',
                      text: 'Storage',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableHeader',
              attrs: {
                colwidth: [466],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'text',
                      text: 'Notes',
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'tableRow',
          content: [
            {
              type: 'tableCell',
              attrs: {
                colwidth: [149],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':confluence:',
                        id: 'e09fe869-53a2-4e89-be70-3a1e4c48dc63',
                        text: ':confluence:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' Confluence',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [108],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':check_mark:',
                        id: 'atlassian-check_mark',
                        text: ':check_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' ',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [107],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':check_mark:',
                        id: 'atlassian-check_mark',
                        text: ':check_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' ',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [129],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' CXHTML',
                    },
                  ],
                },
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'status',
                      attrs: {
                        text: 'Baby steps',
                        color: 'yellow',
                        localId: '1b1f63fd-bbbd-4106-ba34-cf41d25cc71d',
                        style: 'bold',
                      },
                    },
                    {
                      type: 'text',
                      text: ' FY20?',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [466],
              },
              content: [
                {
                  type: 'bulletList',
                  content: [
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl, venenatis eget auctor vitae, venenatis quis lorem. Suspendisse maximus tortor vel dui tincidunt cursus.',
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl, venenatis eget auctor vitae, venenatis quis lorem. Suspendisse maximus tortor vel dui tincidunt cursus. Vestibulum magna nibh, auctor.',
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'tableRow',
          content: [
            {
              type: 'tableCell',
              attrs: {
                colwidth: [149],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':jira:',
                        id: 'b59bb68f-8d88-4f87-820b-d8b946390b72',
                        text: ':jira:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' Jira',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [108],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':check_mark:',
                        id: 'atlassian-check_mark',
                        text: ':check_mark:',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [107],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':check_mark:',
                        id: 'atlassian-check_mark',
                        text: ':check_mark:',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [129],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' Wiki',
                    },
                  ],
                },
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'status',
                      attrs: {
                        text: 'In progress',
                        color: 'yellow',
                        localId: 'c9c84f05-df1d-4418-a35f-15697a81f5ab',
                        style: 'bold',
                      },
                    },
                    {
                      type: 'text',
                      text: ' FY19',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [466],
              },
              content: [
                {
                  type: 'bulletList',
                  content: [
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                            },
                            {
                              type: 'text',
                              text: 'many',
                              marks: [
                                {
                                  type: 'em',
                                },
                              ],
                            },
                            {
                              type: 'text',
                              text:
                                ' Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                            },
                            {
                              type: 'inlineCard',
                              attrs: {
                                url: 'https://wikis.atlassian.net/wiki/spaces/',
                              },
                            },
                            {
                              type: 'text',
                              text: ' ',
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl, venenatis eget auctor vitae, venenatis quis lorem:',
                            },
                          ],
                        },
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'inlineCard',
                              attrs: {
                                url: 'https://wikis.atlassian.net/wiki/spaces',
                              },
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text: 'Tracker: ',
                            },
                            {
                              type: 'text',
                              text: 'Lorem ipsum dolor sit amet',
                              marks: [
                                {
                                  type: 'link',
                                  attrs: {
                                    href:
                                      'https://wikis.atlassian.net/wiki/spaces',
                                  },
                                },
                              ],
                            },
                            {
                              type: 'text',
                              text: ' ',
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'tableRow',
          content: [
            {
              type: 'tableCell',
              attrs: {
                colwidth: [149],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':bitbucket:',
                        id: 'e15e9a4c-7aef-4ec0-acb9-6cca350bcaf8',
                        text: ':bitbucket:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' Bitbucket',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [108],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':check_mark:',
                        id: 'atlassian-check_mark',
                        text: ':check_mark:',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [107],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                  ],
                },
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'status',
                      attrs: {
                        text: 'In progress',
                        color: 'yellow',
                        localId: '6e37a946-cbdb-49d1-b845-095d20f95ea9',
                        style: 'bold',
                      },
                    },
                    {
                      type: 'text',
                      text:
                        ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [129],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' MD',
                    },
                  ],
                },
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'status',
                      attrs: {
                        text: 'Paused',
                        color: 'red',
                        localId: '2fd0c454-6c07-4549-a975-f6b26a76193f',
                        style: 'bold',
                      },
                    },
                    {
                      type: 'text',
                      text: ' Lorem ipsum dolor sit amet, consectetur.',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [466],
              },
              content: [
                {
                  type: 'bulletList',
                  content: [
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl.',
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl, venenatis eget auctor vitae, venenatis quis lorem. Suspendisse maximus tortor vel dui tincidunt cursus',
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl, venenatis ',
                            },
                            {
                              type: 'text',
                              text: 'could ',
                              marks: [
                                {
                                  type: 'em',
                                },
                              ],
                            },
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'tableRow',
          content: [
            {
              type: 'tableCell',
              attrs: {
                colwidth: [149],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':status-page:',
                        id: '1d3c260c-01be-4e87-a63b-1132a84470aa',
                        text: ':status-page:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' Statuspage',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [108],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':check_mark:',
                        id: 'atlassian-check_mark',
                        text: ':check_mark:',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [107],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [129],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' MD',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [466],
              },
              content: [
                {
                  type: 'bulletList',
                  content: [
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl, venenatis',
                            },
                          ],
                        },
                      ],
                    },
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text:
                                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mi nisl, venenatis',
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'tableRow',
          content: [
            {
              type: 'tableCell',
              attrs: {
                colwidth: [149],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':trello:',
                        id: 'b34747f3-67a0-47c6-8675-aef8fc9ee687',
                        text: ':trello:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' Trello',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [108],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                  ],
                },
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'status',
                      attrs: {
                        text: 'Spiking',
                        color: 'blue',
                        localId: '5c87c809-1a6d-4cbb-a35e-1df48afb0ec5',
                        style: 'bold',
                      },
                    },
                    {
                      type: 'text',
                      text: ' ',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [107],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [129],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' MD',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [466],
              },
              content: [
                {
                  type: 'bulletList',
                  content: [
                    {
                      type: 'listItem',
                      content: [
                        {
                          type: 'paragraph',
                          content: [
                            {
                              type: 'text',
                              text: 'Lorem ipsum dolor',
                            },
                          ],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          type: 'tableRow',
          content: [
            {
              type: 'tableCell',
              attrs: {
                colwidth: [149],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'text',
                      text: 'Opsgenie',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [108],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                  ],
                },
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'status',
                      attrs: {
                        text: 'Spiking',
                        color: 'blue',
                        localId: '2e8a32fc-a32a-4e0e-8413-f99b6b0ee684',
                        style: 'bold',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [107],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [129],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [
                    {
                      type: 'emoji',
                      attrs: {
                        shortName: ':cross_mark:',
                        id: 'atlassian-cross_mark',
                        text: ':cross_mark:',
                      },
                    },
                    {
                      type: 'text',
                      text: ' MD?',
                    },
                  ],
                },
              ],
            },
            {
              type: 'tableCell',
              attrs: {
                colwidth: [466],
              },
              content: [
                {
                  type: 'paragraph',
                  content: [],
                },
              ],
            },
          ],
        },
      ],
    },
    {
      type: 'paragraph',
      content: [],
    },
  ],
};

export { wideTableResized };
