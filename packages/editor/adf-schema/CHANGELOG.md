# @atlaskit/adf-schema

## 1.5.5
- [patch] [356de07a87](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/356de07a87):

  - Revert back to number for external media

## 1.5.4
- Updated dependencies [4af5bd2a58](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4af5bd2a58):
  - @atlaskit/editor-json-transformer@4.1.11
  - @atlaskit/editor-test-helpers@7.0.0

## 1.5.3
- [patch] [775da616c6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/775da616c6):

  - [ED-5910] Keep width & height on media node as number

## 1.5.2
- [patch] [e83a441140](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e83a441140):

  - Revert type change to width/height attributes for media node

## 1.5.1
- [patch] [09696170ec](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/09696170ec):

  - Bumps prosemirror-utils to 0.7.6

## 1.5.0
- [minor] [14fe1381ba](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/14fe1381ba):

  - ED-6118: ensure media dimensions are always integers, preventing invalid ADF

## 1.4.6
- [patch] [557a2b5734](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/557a2b5734):

  - ED-5788: bump prosemirror-view and prosemirror-model

## 1.4.5
- [patch] [4552e804d3](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4552e804d3):

  - dismiss StatusPicker if status node is not selected

## 1.4.4
- [patch] [adff2caed7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/adff2caed7):

  - Improve typings

## 1.4.3
- [patch] [d10cf50721](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d10cf50721):

  - added fabric status to ADF full schema

## 1.4.2
- [patch] [478a86ae8a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/478a86ae8a):

  - avoid using the same localId when pasting status

## 1.4.1
- [patch] [2d6d5b6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/2d6d5b6):

  - ED-5379: rework selecting media under the hood; maintain size and layout when copy-pasting

## 1.4.0
- [minor] [c5ee0c8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/c5ee0c8):

  - Added Annotation mark to ADF, editor & renderer

## 1.3.3
- [patch] [060f2da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/060f2da):

  - ED-5991: bumped prosemirror-view to 1.6.8

## 1.3.2
- [patch] [a50c114](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/a50c114):

  - ED-6026: unify attributes for blockCard and inlineCard; allow parseDOM using just 'data' attribute

## 1.3.1
- [patch] [7d9ccd7](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7d9ccd7):

  - fixed copy/paste status from renderer to editor

## 1.3.0
- [minor] [cbcac2e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cbcac2e):

  - Promote smartcard nodes to full schema

## 1.2.0
- [minor] [5b11b69](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5b11b69):

  - Allow mixed of cell types in a table row

## 1.1.0
- [minor] [b9f8a8f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b9f8a8f):

  - Adding alignment options to media

## 1.0.1
- [patch] [d7bfd60](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d7bfd60):

  - Rengenerate JSON schema after moving packages

## 1.0.0
- [major] [1205725](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1205725):

  - Move schema to its own package
