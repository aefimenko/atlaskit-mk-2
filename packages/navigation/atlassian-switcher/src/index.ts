export { default as JiraSwitcher } from './components/jira-switcher';
export {
  default as ConfluenceSwitcher,
} from './components/confluence-switcher';
export { default } from './components/atlassian-switcher';
