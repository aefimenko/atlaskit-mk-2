import styled from 'styled-components';
import { gridSize } from '@atlaskit/theme';

export const SuggestedProductItemText = styled.span`
  margin-right: ${gridSize()}px;
`;
