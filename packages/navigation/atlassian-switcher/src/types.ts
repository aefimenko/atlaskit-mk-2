export interface WithCloudId {
  cloudId: string;
}

export interface RecentContainer {
  name: string;
  url: string;
  objectId: string;
  iconUrl: string;
  type: string;
}

export interface CustomLink {
  key: string;
  label: string;
  link: string;
}
